# Library for selecting folders
from tkinter.filedialog import askdirectory
# Native Python libraries
import os
from PIL import Image

#Converting .png to .ico on the .ico path if .ico does not exist and a .png does 
def pngTOico(icoPath,ico):
	if(os.path.exists(icoPath+ico+"/"+ico+".png")==True):
		print(icoPath+ico+"/"+ico+".png")
	if(os.path.exists(icoPath+ico+"/"+ico+".ico")==True):
		print('Already exist: ' +ico+'.ico')
	if(os.path.exists(icoPath+ico+"/"+ico+".ico")==False):
		icon_sizes = [(16,16),(32,32),(48, 48),(64,64),(124,124),(256,256),(512,512)]
		print("Generating icon: "+ico+".ico")
		img = Image.open(r""+icoPath+ico+"/"+ico+'.png')
		img.save(r""+icoPath+"/"+ico+"/"+ico+'.ico', sizes=icon_sizes)

#If there exist an icon folder with the exactly name of a folder in 
def comparator(icoPath, destPath, destFolder, ico):
	for i in range(len(ico)):
		for j in range(len(destFolder)):
			if (ico[i] == destFolder[j]):
				# Build a .ini in the folder that will get the .ico assigned with the information of where the .ico is
				if (os.path.exists(destPath + destFolder[j] + "/" + "desktop.ini")==False):
					#Calling the method to create .ico if .png already exist on the icon folder
					pngTOico(icoPath,ico[i])
                    #cmd opens and start writting the desktop.ini 
					desktop = open(destPath + destFolder[j] + "/" + "desktop.ini",mode="w+")
					desktop = open(destPath + destFolder[j] + "/" + "desktop.ini",mode="r+")
                    #writes the path of the icon on the desktop.ini
					desktop.write("[.ShellClassInfo]\nIconResource=" + icoPath + ico[i] + "/" + ico[i] + ".ico\n[ViewState]\nMode=\nVid=\nFolderType=Videos")
					
					#Changes the attributs of the folder
					os.chdir(destPath)
					os.system("cmd /c attrib +R \"" + destFolder[j] + "\"")
					os.system("cmd /c attrib \"" + destFolder[j] + "\"")
					#Changes the attributs of the .ini 
					os.chdir(destPath + "/" + destFolder[j])
					os.system("cmd /c attrib -S " + "desktop.ini")
					os.system("cmd /c attrib +H +S " + "desktop.ini")
					os.system("cmd /c attrib " + "desktop.ini")

#### MAIN ####
# Path to the .ico files
icoPath = askdirectory(title='Select the folder with the .ico files')
#You can also coment the line before and put after this the folder you would use frecuently as Icon folder
#f.e.:
#icoPath = "K:/IconFolders/FolderThatAlreadyHasTheIcon"
icoPath = icoPath + '/'

aux = [ f.path for f in os.scandir(icoPath) if f.is_dir() ]

subFoldersico = []
for i in range(len(aux)):
	subFoldersico.append(aux[i].split("/")[-1])

#Read the files of the folder selected the .ico that is named equal to this folder
entries = []
ico = []
for i in range(len(subFoldersico)):
	entries = os.listdir(icoPath + subFoldersico[i])
	for j in range(len(entries)):
		if (entries[j][0:-4] == subFoldersico[i]):
			ico.append(entries[j][0:-4])

#Path to the destination folder wich will have the .ico assigned
destPath = askdirectory(title='Select the folder to modify')
#You can also coment the line before and put after this the folder you would use frecuently as Icon folder
#f.e.:
#destPath="K:/destinationFolders/MyFolderWithNewIcon"
destPath = destPath + '/'

subFolders = [ f.path for f in os.scandir(destPath) if f.is_dir() ]
destFolder = [];
for i in range(len(subFolders)):
	destFolder.append(subFolders[i].split("/")[-1])

# Execute the function comparator to get the icon on the new folder
comparator(icoPath, destPath, destFolder, ico)

#exit()
